UNAME := $(shell uname)

INCLUDE := \
 -I./third_parties/stb/ \
 -I./third_parties/cglm/include/cglm/ \

LIB := \
  -L./third_parties/cglm/build/ \
  -lcglm

MSYS_MINGW64_INCLUDE := \
	-I/mingw64/include/ \
	-I/mingw64/include/mesa/GL/ \
	-I./third_parties/stb/ \
	-I./third_parties/cglm/include/cglm/ \

MSYS_MINGW64_LIB := \
	-L./third_parties/cglm/build/ \
	-L/mingw64/lib/ \
	-lcglm \
	-lSDL2 \
	-lopengl32 \
	-lglew32	
all:
ifeq ($(UNAME), Linux)
	gcc -o main main.c $(INCLUDE) -lSDL2 -lGL -lGLEW -lm $(LIB)
else
	gcc -o main main.c $(MSYS_MINGW64_INCLUDE) -lm $(MSYS_MINGW64_LIB)
endif

run:
	make
	./main
