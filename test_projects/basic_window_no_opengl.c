#include<stdio.h>
#include<stdbool.h>
#include<SDL2/SDL.h>

int main()
{
	SDL_Window *window = NULL;
	int window_width = 200;
	int window_height = 200;
	Uint32 window_flags = SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE;
	window =  SDL_CreateWindow("SDL2 window", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width, window_height, window_flags);

	Uint32 start;
	int target_fps = 60;
	bool is_running = true;
	SDL_Event event;

	while(is_running)
	{
		start = SDL_GetTicks();
		
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_QUIT:
					is_running = false;
				break;
			}
		}

		Uint32 ms = 1000 / target_fps;

		if(ms > (SDL_GetTicks() - start))
		{
			Uint32 delay = ms - (SDL_GetTicks() - start);
			SDL_Delay(delay);
		}


	} // is_running

	SDL_DestroyWindow(window);
	SDL_Quit();
	
	return 0;
}

