/*
License under zlib license
Copyright (C) 2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "khkSDL2GL.h"

double to_radian()
{ 
	return PI / 180.0f;
}

double to_degree()
{
	return 180.0f / PI;
}

void read_file(const char *src_file, char *buff_str, size_t buff_len)
{
	FILE *file;
	file = fopen(src_file, "r");
	
	int c = 0;
	int index = 0;
	
	if(!file)
	{
		printf("file not found:%s\n", src_file);
	}

	while((c = fgetc(file)) != EOF)
	{
		char ch = (char)c;
		buff_str[index] = ch;
		++index;
		buff_str[index] = '\0';
	}
	
	size_t str_len = strlen(buff_str);
	if(str_len < buff_len)
	{
		buff_str = realloc(buff_str, str_len);
	}
	
	fclose(file);
}

int shader_success_state;
char shader_info_log[512];
void link_shader(unsigned int *shader_program, const char *vertex_shader_source, const char *fragment_shader_source)
{
	unsigned int vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_shader_source, NULL);
	glCompileShader(vertex_shader);
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &shader_success_state);

	if(!shader_success_state)
	{
		glGetShaderInfoLog(vertex_shader, 512, NULL, shader_info_log);
		printf("vertex shader compile error:%s\n", shader_info_log);
	}

	unsigned int fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_shader_source, NULL);
	glCompileShader(fragment_shader);
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &shader_success_state);

	if(!shader_success_state)
	{
		glGetShaderInfoLog(fragment_shader, 512, NULL, shader_info_log);
		printf("fragment shader compile error:%s\n", shader_info_log);
	}

	*shader_program = glCreateProgram();
	glAttachShader(*shader_program, vertex_shader);
	glAttachShader(*shader_program, fragment_shader);
	glLinkProgram(*shader_program);
	glGetProgramiv(*shader_program, GL_LINK_STATUS, &shader_success_state);

	if (!shader_success_state) 
	{
		glGetProgramInfoLog(*shader_program, 512, NULL, shader_info_log);
		printf("shader linking error:%s\n", shader_info_log);
	}

	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
}

void shader_set_mat4(unsigned int shader_program, const char *uniform_name, mat4 mat)
{
	unsigned int uniform_location = glGetUniformLocation(shader_program, uniform_name);
	glUniformMatrix4fv(uniform_location, 1, GL_FALSE, (const GLfloat*)mat);
}

void shader_set_vec3(unsigned int shader_program, const char *uniform_name, vec3 vec)
{
	unsigned int uniform_location = glGetUniformLocation(shader_program, uniform_name);
	glUniform3fv(uniform_location, 1, vec);
}

void shader_set_int(unsigned int shader_program, const char *uniform_name, int value)
{
	unsigned int uniform_location = glGetUniformLocation(shader_program, uniform_name);
	glUniform1i(uniform_location, value);
}

void shader_set_float(unsigned int shader_program, const char *uniform_name, float value)
{
	unsigned int uniform_location = glGetUniformLocation(shader_program, uniform_name);
	glUniform1f(uniform_location, value);
}

struct Image
{
	unsigned char *image_data;
	int width;
	int height;
	int channels;
};

struct Image* load_image(const char* image_src)
{
	struct Image *image = malloc(sizeof(struct Image));
	image->image_data = stbi_load(
			image_src, 
			&image->width, 
			&image->height, 
			&image->channels,
			0);

	if(!image->image_data)
	{
		printf("failed to load image:%s\n", image_src);
		exit(1);
	}
	
	return image;
}

void load_texture(unsigned int *texture, struct Image *image)
{
	glGenTextures(1, texture);
	glBindTexture(GL_TEXTURE_2D, *texture);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(image->image_data)
	{
		if(image->channels == 3)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->image_data);
		}
		else if(image->channels == 4)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->width, image->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->image_data);
		}

		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		printf("failed to load texture\n");
	}

	stbi_image_free(image->image_data);
}

int main()
{
	int window_width = 800;
	int window_height = 800;

	const char *texture_fs = malloc(512);
	const char *transform_vs = malloc(512);
	const char *transform_n_vs = malloc(512);
	const char *color_fs = malloc(512);
	const char *lighting_vs = malloc(512);
	const char *lighting_fs = malloc(1024 * 5);
	read_file("./shaders/texture.fs", (char*)texture_fs, 512);
	read_file("./shaders/transform.vs", (char*)transform_vs, 512);
	read_file("./shaders/transform_n.vs", (char*)transform_n_vs, 512);
	read_file("./shaders/color.fs", (char*)color_fs, 512);
	read_file("./shaders/lighting.vs", (char*)lighting_vs, 512);
	read_file("./shaders/multiple_lighting.fs", (char*)lighting_fs, 1024 * 5);

	SDL_Window *window = NULL;

	if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		fprintf(stderr, "SDL failed to initialize: %s\n", SDL_GetError());
		return 1;
	}
	
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);

	Uint32 window_flags = SDL_WINDOW_OPENGL;

	window = SDL_CreateWindow(
			"SDL2 Window", 
			SDL_WINDOWPOS_CENTERED, 
			SDL_WINDOWPOS_CENTERED, 
			window_width, 
			window_height, 
			window_flags);

	SDL_GLContext context = SDL_GL_CreateContext(window);

	GLenum err = glewInit();
	if(err != GLEW_OK)
	{
		printf("GLEW failed initialize!");
		exit(1);
	}

	glEnable(GL_DEPTH_TEST);

	unsigned int transform_shader_program;
	link_shader(&transform_shader_program, transform_vs, texture_fs);
	unsigned int transform_shader_program_cube;
	link_shader(&transform_shader_program_cube, transform_n_vs, texture_fs);
	unsigned int light_shader_program;
	link_shader(&light_shader_program, lighting_vs, lighting_fs);
	unsigned int color_shader_program;
	link_shader(&color_shader_program, transform_n_vs, color_fs);

	free((char*)texture_fs);
	free((char*)transform_vs);
	free((char*)transform_n_vs);
	free((char*)color_fs);
	free((char*)lighting_fs);

	struct Image *wall_image = load_image("./resources/images/wall.jpg");
	struct Image *container_diffuse = load_image("./resources/images/container2.png");
	struct Image *container_specular = load_image("./resources/images/container2_specular.png");
	
	// rectangle
	float vertices[] = {
        // positions          // colors           // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // top left 
	};
	unsigned int indices[] = {  // note that we start from 0!
		0, 1, 3,   // first triangle
		1, 2, 3    // second triangle
	};  

	unsigned int vbo, vao, ebo;
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);
	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ARRAY_BUFFER, 0); 
	glBindVertexArray(0); 	

	float cube_vertices[] = {
		// positions           // normals            // texture coords
		-0.5f, -0.5f, -0.5f,   0.0f,  0.0f, -1.0f,   0.0f,  0.0f,
		 0.5f, -0.5f, -0.5f,   0.0f,  0.0f, -1.0f,   1.0f,  0.0f,
		 0.5f,  0.5f, -0.5f,   0.0f,  0.0f, -1.0f,   1.0f,  1.0f,
		 0.5f,  0.5f, -0.5f,   0.0f,  0.0f, -1.0f,   1.0f,  1.0f,
		-0.5f,  0.5f, -0.5f,   0.0f,  0.0f, -1.0f,   0.0f,  1.0f,
		-0.5f, -0.5f, -0.5f,   0.0f,  0.0f, -1.0f,   0.0f,  0.0f,

		-0.5f, -0.5f,  0.5f,   0.0f,  0.0f,  1.0f,   0.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,   0.0f,  0.0f,  1.0f,   1.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,   0.0f,  0.0f,  1.0f,   1.0f,  1.0f,
		 0.5f,  0.5f,  0.5f,   0.0f,  0.0f,  1.0f,   1.0f,  1.0f,
		-0.5f,  0.5f,  0.5f,   0.0f,  0.0f,  1.0f,   0.0f,  1.0f,
		-0.5f, -0.5f,  0.5f,   0.0f,  0.0f,  1.0f,   0.0f,  0.0f,

		-0.5f,  0.5f,  0.5f, - 1.0f,  0.0f,  0.0f,   1.0f,  0.0f,
		-0.5f,  0.5f, -0.5f, - 1.0f,  0.0f,  0.0f,   1.0f,  1.0f,
		-0.5f, -0.5f, -0.5f, - 1.0f,  0.0f,  0.0f,   0.0f,  1.0f,
		-0.5f, -0.5f, -0.5f, - 1.0f,  0.0f,  0.0f,   0.0f,  1.0f,
		-0.5f, -0.5f,  0.5f, - 1.0f,  0.0f,  0.0f,   0.0f,  0.0f,
		-0.5f,  0.5f,  0.5f, - 1.0f,  0.0f,  0.0f,   1.0f,  0.0f,

		 0.5f,  0.5f,  0.5f,   1.0f,  0.0f,  0.0f,   1.0f,  0.0f,
		 0.5f,  0.5f, -0.5f,   1.0f,  0.0f,  0.0f,   1.0f,  1.0f,
		 0.5f, -0.5f, -0.5f,   1.0f,  0.0f,  0.0f,   0.0f,  1.0f,
		 0.5f, -0.5f, -0.5f,   1.0f,  0.0f,  0.0f,   0.0f,  1.0f,
		 0.5f, -0.5f,  0.5f,   1.0f,  0.0f,  0.0f,   0.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,   1.0f,  0.0f,  0.0f,   1.0f,  0.0f,

		-0.5f, -0.5f, -0.5f,   0.0f, -1.0f,  0.0f,   0.0f,  1.0f,
		 0.5f, -0.5f, -0.5f,   0.0f, -1.0f,  0.0f,   1.0f,  1.0f,
		 0.5f, -0.5f,  0.5f,   0.0f, -1.0f,  0.0f,   1.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,   0.0f, -1.0f,  0.0f,   1.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,   0.0f, -1.0f,  0.0f,   0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,   0.0f, -1.0f,  0.0f,   0.0f,  1.0f,

		-0.5f,  0.5f, -0.5f,   0.0f,  1.0f,  0.0f,   0.0f,  1.0f,
		 0.5f,  0.5f, -0.5f,   0.0f,  1.0f,  0.0f,   1.0f,  1.0f,
		 0.5f,  0.5f,  0.5f,   0.0f,  1.0f,  0.0f,   1.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,   0.0f,  1.0f,  0.0f,   1.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,   0.0f,  1.0f,  0.0f,   0.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,   0.0f,  1.0f,  0.0f,   0.0f,  1.0f
	};

	unsigned int cube_vao, cube_vbo;
	glGenVertexArrays(1, &cube_vao);
	glGenBuffers(1, &cube_vbo);
	glBindVertexArray(cube_vao);
	glBindBuffer(GL_ARRAY_BUFFER, cube_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 *sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 *sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 *sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);

	unsigned int texture;
	load_texture(&texture, wall_image);

	unsigned int diffuse_map;
	load_texture(&diffuse_map, container_diffuse);
	unsigned int specular_map;
	load_texture(&specular_map, container_specular);

	bool is_running = true;
	SDL_Event event;

	float degree = 0.0f;

	vec3 camera_position = {0.0f, 0.0f, 3.0f};
	vec3 camera_front = {0.0f, 0.0f, -1.0f};
	vec3 camera_up = {0.0f, 1.0f, 0.0f};
	float camera_fov = 80.0f;

	vec3 object_color = {1.0f, 0.5f, 0.31f};
	vec3 object_position = {0.0f, 1.0f, -0.5f};

	vec3 light_color = {1.0f, 1.0f, 1.0f};
	vec3 light_position = {0.0f, 1.0f, 0.5f};
	vec3 light_direction = {-0.2f, -1.0f, -0.3f}; // directional light test

	vec3 cube_positions[] = 
	{
		{ 0.0f,  0.0f,  0.0f},
		{ 2.0f,  5.0f, -15.0f},
		{-1.5f, -2.2f, -2.5f},
		{-3.8f, -2.0f, -12.3f},
		{ 2.4f, -0.4f, -3.5f},
		{-1.7f,  3.0f, -7.5f},
		{ 1.3f, -2.0f, -2.5f},
		{ 1.5f,  2.0f, -2.5f},
		{ 1.5f,  0.2f, -1.5f},
		{-1.3f,  1.0f, -1.5f}
	};

	vec3 point_light_positions[] = 
	{
		{ 0.7f,  0.2f,  2.0f},
		{ 2.3f, -3.3f, -4.0f},
		{-4.0f,  2.0f, -12.0f},
		{ 0.0f,  0.0f, -3.0f}
	};

	vec3 point_light_colors[] = 
	{
		{1.0f, 0.0f, 0.0f},
		{0.8f, 0.8f, 0.8f},
		{0.8f, 0.8f, 0.8f},
		{0.0f, 0.0f, 1.0f}
	};
	
	while(is_running)
	{	
		while(SDL_PollEvent(&event))
		{
			if(event.type == SDL_QUIT)
			{
				is_running = false;
			}
		}
	
		// render test
		// start render
		glClearColor(0.2f, 0.3f, 0.3f, 0.1f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, window_width, window_height);

		degree += 1.0f;	

		mat4 projection = GLM_MAT4_IDENTITY_INIT;
		glm_perspective(camera_fov * to_radian(), (float)window_width/ (float)window_height, 0.1f, 100.0f, projection);

		mat4 view = GLM_MAT4_IDENTITY_INIT;
		vec3 center_view = GLM_VEC3_ZERO_INIT;
		glm_vec3_add(camera_position, camera_front, center_view);
		glm_lookat(camera_position, center_view, camera_up, view);

		glUseProgram(transform_shader_program);

		// for(int i = 0; i < 2; ++i) // instancing test
		// {

		// 	mat4 model = GLM_MAT4_IDENTITY_INIT;
		// 	glm_rotate(model, degree * to_radian(), (vec3){1.0f, 0.0f, 0.0f});
		// 	glm_translate(model, (vec3){(float)i, 0.0f, 0.0f});

		// 	mat4 transform = GLM_MAT4_IDENTITY_INIT;
		// 	glm_mat4_mulN((mat4 *[]){&projection, &view, &model}, 3, transform);

		// 	shader_set_mat4(transform_shader_program, "shader_transform", transform);

		// 	glActiveTexture(GL_TEXTURE0);
		// 	glBindTexture(GL_TEXTURE_2D, texture);

		// 	glBindVertexArray(vao);
		// 	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		// }

		// white light
		{ 
			glUseProgram(color_shader_program);	

			for(int i = 0; i < 4; ++i)
			{
				mat4 model = GLM_MAT4_IDENTITY_INIT;
				glm_translate(model, point_light_positions[i]);
				glm_scale(model, (vec3){0.2f, 0.2f, 0.2f});

				mat4 transform = GLM_MAT4_IDENTITY_INIT;
				glm_mat4_mulN((mat4 *[]){&projection, &view, &model}, 3, transform);

				shader_set_vec3(color_shader_program, "object_color", point_light_colors[i]);
				shader_set_vec3(color_shader_program, "light_color", point_light_colors[i]);
				shader_set_mat4(color_shader_program, "shader_transform", transform);

				glBindVertexArray(cube_vao);
				glDrawArrays(GL_TRIANGLES, 0, 36);
			}
		}

		// orange object
		{ 
			glUseProgram(light_shader_program);
			shader_set_int(light_shader_program, "material.diffuse", 0);
			shader_set_int(light_shader_program, "material.specular", 1);

			for(int i = 0; i < 10; ++i)
			{
				mat4 model = GLM_MAT4_IDENTITY_INIT;
				glm_rotate_at(model, cube_positions[i], degree * to_radian(), (vec3){0.0f, 1.0f, 0.0f});
				glm_translate(model, cube_positions[i]);

				vec3 ambient = GLM_VEC3_ZERO_INIT;
				glm_vec3_mul(light_position, (vec3){0.2f, 0.2f, 0.2f}, ambient);

				shader_set_vec3(light_shader_program, "view_position", camera_position);

				// directional light
				shader_set_vec3(light_shader_program, "directional_light.direction", (vec3){-0.2f, -1.0f, -0.3f});
				shader_set_vec3(light_shader_program, "directional_light.ambient", (vec3){0.05f, 0.05f, 0.05f});
				shader_set_vec3(light_shader_program, "directional_light.diffuse", (vec3){0.4f, 0.4f, 0.4f});
				shader_set_vec3(light_shader_program, "directional_light.specular", (vec3){0.5f, 0.5f, 0.5f});

				// point light 1
				shader_set_vec3(light_shader_program, "point_lights[0].position", point_light_positions[0]);
				shader_set_vec3(light_shader_program, "point_lights[0].ambient", (vec3){0.05f, 0.05f, 0.05f});
				shader_set_vec3(light_shader_program, "point_lights[0].diffuse", point_light_colors[0]);
				shader_set_vec3(light_shader_program, "point_lights[0].specular", (vec3){1.0f, 1.0f, 1.0f});
				shader_set_float(light_shader_program, "point_lights[0].constant", 1.0f);
				shader_set_float(light_shader_program, "point_lights[0].linear", 0.09f);
				shader_set_float(light_shader_program, "point_lights[0].quadratic", 0.032f);

				// point light 2
				shader_set_vec3(light_shader_program, "point_lights[1].position", point_light_positions[1]);
				shader_set_vec3(light_shader_program, "point_lights[1].ambient", (vec3){0.05f, 0.05f, 0.05f});
				shader_set_vec3(light_shader_program, "point_lights[1].diffuse", point_light_colors[1]);
				shader_set_vec3(light_shader_program, "point_lights[1].specular", (vec3){1.0f, 1.0f, 1.0f});
				shader_set_float(light_shader_program, "point_lights[1].constant", 1.0f);
				shader_set_float(light_shader_program, "point_lights[1].linear", 0.09f);
				shader_set_float(light_shader_program, "point_lights[1].quadratic", 0.032f);

				// point light 3
				shader_set_vec3(light_shader_program, "point_lights[2].position", point_light_positions[2]);
				shader_set_vec3(light_shader_program, "point_lights[2].ambient", (vec3){0.05f, 0.05f, 0.05f});
				shader_set_vec3(light_shader_program, "point_lights[2].diffuse", point_light_colors[2]);
				shader_set_vec3(light_shader_program, "point_lights[2].specular", (vec3){1.0f, 1.0f, 1.0f});
				shader_set_float(light_shader_program, "point_lights[2].constant", 1.0f);
				shader_set_float(light_shader_program, "point_lights[2].linear", 0.09f);
				shader_set_float(light_shader_program, "point_lights[2].quadratic", 0.032f);

				// point light 4
				shader_set_vec3(light_shader_program, "point_lights[3].position", point_light_positions[3]);
				shader_set_vec3(light_shader_program, "point_lights[3].ambient", (vec3){0.05f, 0.05f, 0.05f});
				shader_set_vec3(light_shader_program, "point_lights[3].diffuse", point_light_colors[3]);
				shader_set_vec3(light_shader_program, "point_lights[3].specular", (vec3){1.0f, 1.0f, 1.0f});
				shader_set_float(light_shader_program, "point_lights[3].constant", 1.0f);
				shader_set_float(light_shader_program, "point_lights[3].linear", 0.09f);
				shader_set_float(light_shader_program, "point_lights[3].quadratic", 0.032f);

				// spot light
				shader_set_vec3(light_shader_program, "spot_light.position", camera_position);
				shader_set_vec3(light_shader_program, "spot_light.direction", camera_front);
				shader_set_vec3(light_shader_program, "spot_light.ambient", (vec3){0.0f, 0.0f, 0.0f});
				shader_set_vec3(light_shader_program, "spot_light.diffuse", (vec3){1.0f, 1.0f, 1.0f});
				shader_set_vec3(light_shader_program, "spot_light.specular", (vec3){1.0f, 1.0f, 1.0f});
				shader_set_float(light_shader_program, "spot_light.constant", 1.0f);
				shader_set_float(light_shader_program, "spot_light.linear", 0.09f);
				shader_set_float(light_shader_program, "spot_light.quadratic", 0.032f);
				shader_set_float(light_shader_program, "spot_light.cut_off", cos(12.5f * to_radian()));
				shader_set_float(light_shader_program, "spot_light.outer_cut_off",cos(15.0f * to_radian()));     
				
				shader_set_float(light_shader_program, "material.shininess", 32.0f);

				shader_set_mat4(light_shader_program, "projection", projection);
				shader_set_mat4(light_shader_program, "view", view);
				shader_set_mat4(light_shader_program, "model", model);

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, diffuse_map);
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, specular_map);

				glBindVertexArray(cube_vao);
				glDrawArrays(GL_TRIANGLES, 0, 36);
			}

		}

		SDL_GL_SwapWindow(window);
	}

	free(wall_image);
	free(container_diffuse);
	free(container_specular);

	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ebo);
	glDeleteVertexArrays(1, &cube_vao);
	glDeleteBuffers(1, &cube_vbo);
	
	// SDL_Delay(3000);
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
