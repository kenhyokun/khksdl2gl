#version 330 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal_coord;
layout (location = 2) in vec2 texture_coord;

out vec3 frag_pos;
out vec3 normal;
out vec2 uv;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	frag_pos = vec3(model * vec4(pos, 1.0));
	normal = mat3(transpose(inverse(model))) * normal_coord;
	uv = texture_coord;

	gl_Position = projection * view * vec4(frag_pos, 1.0);
}
