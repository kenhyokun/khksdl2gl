#version 330 core
out vec4 frag_color;
  
in vec3 shader_color;
in vec2 shader_tex_coord;

uniform sampler2D shader_texture;

void main()
{
    frag_color = texture(shader_texture, shader_tex_coord);
}
