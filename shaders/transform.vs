#version 330 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 color;
layout (location = 2) in vec2 tex_coord;

out vec3 shader_color;
out vec2 shader_tex_coord;

uniform mat4 shader_transform;

void main()
{
    gl_Position = shader_transform * vec4(pos, 1.0f);
    shader_color = color;
    shader_tex_coord = tex_coord;
}
