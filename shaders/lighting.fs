#version 330 core
out vec4 frag_color;

struct Material
{
	sampler2D diffuse;
	sampler2D specular;
	float shininess;
};

struct Light
{
	vec3 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct DirectionalLight
{
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct PointLight
{
	vec3 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float constant;
	float linear;
	float quadratic;
};

struct FlashLight
{
	vec3 position;
	vec3 direction;
	float cut_off;
	float outer_cut_off;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float constant;
	float linear;
	float quadratic;
};

in vec3 frag_pos;
in vec3 normal;
in vec2 uv;

uniform vec3 view_position;
uniform Material material;
// uniform Light light; // light test
// uniform DirectionalLight light; // directional light test
// uniform PointLight light; // point light test
uniform FlashLight light; // light test

void main()
{
	// // base light test
	// vec3 diffuse_map = texture(material.diffuse, uv).rgb;
	// vec3 ambient = light.ambient * diffuse_map;	
	// vec3 normalized = normalize(normal);
	// vec3 light_direction = normalize(light.position - frag_pos);
	// float diff = max(dot(normalized, light_direction), 0.0);
	// vec3 diffuse =  light.diffuse * diff * diffuse_map;

	// vec3 specular_map = texture(material.specular, uv).rgb;
	// vec3 view_direction = normalize(view_position - frag_pos);
	// vec3 reflect_direction = reflect(-light_direction, normalized);
	// float spec = pow(max(dot(view_direction, reflect_direction), 0.0), material.shininess);
	// vec3 specular = light.specular * spec * specular_map; 

	// vec3 result = ambient + diffuse + specular;
	// frag_color = vec4(result, 1.0);


	// // directional light test
	// vec3 diffuse_map = texture(material.diffuse, uv).rgb;
	// vec3 ambient = light.ambient * diffuse_map;	
	// vec3 normalized = normalize(normal);
	// vec3 light_direction = normalize(-light.direction);
	// float diff = max(dot(normalized, light_direction), 0.0);
	// vec3 diffuse =  light.diffuse * diff * diffuse_map;

	// vec3 specular_map = texture(material.specular, uv).rgb;
	// vec3 view_direction = normalize(view_position - frag_pos);
	// vec3 reflect_direction = reflect(-light_direction, normalized);
	// float spec = pow(max(dot(view_direction, reflect_direction), 0.0), material.shininess);
	// vec3 specular = light.specular * spec * specular_map; 

	// vec3 result = ambient + diffuse + specular;
	// frag_color = vec4(result, 1.0);

	// // point light test
	// vec3 diffuse_map = texture(material.diffuse, uv).rgb;
	// vec3 ambient = light.ambient * diffuse_map;	
	// vec3 normalized = normalize(normal);
	// vec3 light_direction = normalize(light.position - frag_pos);
	// float diff = max(dot(normalized, light_direction), 0.0);
	// vec3 diffuse =  light.diffuse * diff * diffuse_map;

	// vec3 specular_map = texture(material.specular, uv).rgb;
	// vec3 view_direction = normalize(view_position - frag_pos);
	// vec3 reflect_direction = reflect(-light_direction, normalized);
	// float spec = pow(max(dot(view_direction, reflect_direction), 0.0), material.shininess);
	// vec3 specular = light.specular * spec * specular_map; 

	// float distance = length(light.position - frag_pos);
	// float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
	// ambient *= attenuation;
	// diffuse *= attenuation;
	// specular *= attenuation;

	// vec3 result = ambient + diffuse + specular;
	// frag_color = vec4(result, 1.0);


	// flash light test
	vec3 diffuse_map = texture(material.diffuse, uv).rgb;
	vec3 ambient = light.ambient * diffuse_map;	
	vec3 normalized = normalize(normal);
	vec3 light_direction = normalize(light.position - frag_pos);
	float diff = max(dot(normalized, light_direction), 0.0);
	vec3 diffuse =  light.diffuse * diff * diffuse_map;

	vec3 specular_map = texture(material.specular, uv).rgb;
	vec3 view_direction = normalize(view_position - frag_pos);
	vec3 reflect_direction = reflect(-light_direction, normalized);
	float spec = pow(max(dot(view_direction, reflect_direction), 0.0), material.shininess);
	vec3 specular = light.specular * spec * specular_map; 

	// spot light (soft edges)
	float theta = dot(light_direction, normalize(-light.direction));
	float epsilon = (light.cut_off - light.outer_cut_off);
	float intensity = clamp ((theta - light.outer_cut_off) / epsilon, 0.0, 1.0);
	diffuse *= intensity;
	specular *= intensity;

	float distance = length(light.position - frag_pos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;

	vec3 result = ambient + diffuse + specular;
	frag_color = vec4(result, 1.0);
}	
