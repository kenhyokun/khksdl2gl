/*
License under zlib license
Copyright (C) 2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include<GL/glew.h>
#include<GL/gl.h>

#if defined _WIN32
#define SDL_MAIN_HANDLED
#endif
#include<SDL2/SDL.h>

#include<stdio.h>
#include<stdbool.h>
#include<math.h> // add -lm on gcc linker
#include<cglm.h>


#define STB_IMAGE_IMPLEMENTATION
#include<stb_image.h>

#define PI 3.14159265359

typedef vec3 v3;

double to_radian();
double to_degree();

void read_file(const char *src_file, char *buff_str, size_t buff_len);
void link_shader(unsigned int *shader_program, const char *vertex_shader_source, const char *fragment_shader_source);

void shader_set_mat4(unsigned int shader_program, const char *uniform_name, mat4 mat);
void shader_set_v3(unsigned int shader_program, const char *uniform_name, v3 vec);
void shader_set_int(unsigned int shader_program, const char *uniform_name, int value);
void shader_set_float(unsigned int shader_program, const char *uniform_name, float value);

struct ImageData
{
	unsigned char *image;
	int width;
	int height;
	int channels;
};
typedef struct ImageData ImageData;

struct Texture
{
	ImageData *image_data;
	unsigned int texture_data;
};
typedef struct Texture Texture;

Texture* load_texture(const char *image_src);
